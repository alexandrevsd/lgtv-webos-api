export type ReturnValue = {
  returnValue: boolean;
};

export type AppLaunchReturn = {
  id: string;
  returnValue: boolean;
  sessionId: string;
};

export type NotificationReturn = {
  toastId: string;
  returnValue: boolean;
};

export type AudioStatusReturn = {
  returnValue: boolean;
  scenario: string;
  volume: number;
  mute: boolean;
};

export type ErrorReturn = {
  returnValue: false;
  errorCode: number;
  errorText: string;
};

export type ChannelReturn = {
  channelId: string;
  signalChannelId: string;
  channelModeId: number;
  channelModeName: string;
  channelTypeId: number;
  channelTypeName: string;
  channelNumber: number;
  channelName: string;
  physicalNumber: number;
  isSkipped: boolean;
  isLocked: boolean;
  isDescrambled: boolean;
  isScrambled: boolean;
  isFineTuned: boolean;
  isInvisible: boolean;
  isHEVCChannel: boolean;
  favoriteGroup: null | string;
  hybridtvType: null | string;
  returnValue: boolean;
};

export type SocketReturn = {
  returnValue: boolean;
  socketPath: string;
};

export type InputListReturn = {
  devices: Device[];
  returnValue: boolean;
};

export type Device = {
  id: string;
  label: string;
  port: number;
  appId: string;
  icon: string;
  modified: boolean;
  autoav: boolean;
  currentTVStatus: string;
  subList: any[];
  subCount: number;
  connected: boolean;
  favorite: boolean;
};

export type SoftwareInfos = {
  returnValue: boolean;
  product_name: string;
  model_name: string;
  sw_type: string;
  major_ver: string;
  minor_ver: string;
  country: string;
  device_id: string;
  auth_flag: string;
  ignore_disable: string;
  eco_info: string;
  config_key: string;
  language_code: string;
};

export type ServiceListReturn = {
  services: Service[];
  returnValue: boolean;
};

export type Service = {
  name: string;
  version: number;
};

export type LaunchPointsList = {
  subscribed: boolean;
  launchPoints: LaunchPoint[];
  caseDetail: CaseDetail;
  returnValue: boolean;
};

export type CaseDetail = {
  serviceCountryCode: string;
  localeCode: string;
  change: any[];
  broadcastCountryCode: string;
};

export type LaunchPoint = {
  systemApp: boolean;
  removable: boolean;
  relaunch: boolean;
  largeIcon: string;
  id: string;
  title: string;
  bgColor: string;
  iconColor: string;
  appDescription: string;
  lptype: string;
  params: object;
  bgImage: string;
  icon: string;
  launchPointId: string;
  favicon: string;
  imageForRecents: string;
};

export type ForegroundApp = {
  appId: string;
  returnValue: boolean;
  windowId: string;
  processId: string;
};

export type SoundOutput = {
  soundOutput: string;
  returnValue: boolean;
};

export type SystemInfo = {
  features: object;
  receiverType: string;
  modelName: string;
  returnValue: boolean;
};

export type AppsList = {
  apps: App[];
  returnValue: boolean;
};

export type App = {
  defaultWindowType: string;
  version: string;
  mediumIcon: string;
  systemApp: boolean;
  vendor: string;
  miniicon: string;
  hasPromotion: boolean;
  requestedWindowOrientation: string;
  largeIcon: string;
  lockable: boolean;
  transparent: boolean;
  appDescription: string;
  checkUpdateOnLaunch: boolean;
  category: string;
  icon: string;
  launchinnewgroup: boolean;
  handlesRelaunch: boolean;
  id: string;
  inspectable: boolean;
  noSplashOnLaunch: boolean;
  inAppSetting: boolean;
  trustLevel: string;
  title: string;
  deeplinkingParams: string;
  privilegedJail: boolean;
  hardwareFeaturesNeeded: any[];
  visible: boolean;
  noWindow: boolean;
  age: number;
  requiredEULA: string;
  installedTime: number;
  folderPath: string;
  main: string;
  removable: boolean;
  type: string;
  disableBackHistoryAPI: boolean;
  iconColor: string;
  appsize: number;
};
