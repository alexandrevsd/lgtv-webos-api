import WebOsSocket from 'lgtv2';
import wol, { ErrorCallback } from 'wake_on_lan';
import {
  AppLaunchReturn,
  AppsList,
  AudioStatusReturn,
  ChannelReturn,
  ErrorReturn,
  ForegroundApp,
  InputListReturn,
  LaunchPointsList,
  NotificationReturn,
  ReturnValue,
  ServiceListReturn,
  SocketReturn,
  SoftwareInfos,
  SoundOutput,
  SystemInfo,
} from './types';

export default class LgTvWebOsApi {
  private readonly ip: string;
  private readonly macAddress: string | undefined;
  private socket: WebOsSocket | undefined;

  constructor(ip: string, macAddress?: string) {
    this.ip = ip;
    this.macAddress = macAddress;
    this.connect();
  }

  private connect(): void {
    this.socket = new WebOsSocket({ url: `ws://${this.ip}:3000` });
    this.listenSocket();
  }

  private listenSocket(): void {
    if (this.socket) {
      this.socket.on('error', (e: Error) => console.error(e.message));
      this.socket.on('connect', () =>
        console.log(`Connection to TV at "${this.ip}" established !`),
      );
    }
  }

  private request(uri: string, payload?: object): Promise<any> {
    return new Promise((resolve) => {
      if (this.socket) {
        this.socket.request('ssap://' + uri, payload, (err: any, res: any) => {
          resolve(res);
        });
      } else {
        resolve(undefined);
      }
    });
  }

  private wakeOnLan(): Promise<ErrorCallback | undefined> {
    return new Promise((resolve) => {
      if (this.macAddress) {
        wol.wake(this.macAddress, (err) => {
          if (err) console.error(err);
          resolve(err);
        });
      } else {
        resolve(undefined);
      }
    });
  }

  on(event: 'close' | 'connect', callback: () => void) {
    if (this.socket) {
      if (event === 'close') {
        this.socket.on(event, callback);
      } else if (event === 'connect') {
        this.socket.on(event, callback);
      }
    }
  }

  turnOn(): Promise<ErrorCallback | undefined> {
    return this.wakeOnLan();
  }

  turnOff(): Promise<ReturnValue | undefined> {
    return this.request('system/turnOff');
  }

  openWebBrowserAt(url: string): Promise<AppLaunchReturn | undefined> {
    return this.request('system.launcher/open', { target: url });
  }

  sendNotification(message: string): Promise<NotificationReturn | undefined> {
    return this.request('system.notifications/createToast', { message });
  }

  setMute(muted: boolean): Promise<ReturnValue | undefined> {
    return this.request('audio/setMute', { mute: muted });
  }

  getAudioStatus(): Promise<AudioStatusReturn | undefined> {
    return this.request('audio/getStatus');
  }

  getVolume(): Promise<AudioStatusReturn | undefined> {
    return this.request('audio/getVolume');
  }

  setVolume(volume: number): Promise<ReturnValue | undefined> {
    return this.request('audio/setVolume', { volume });
  }

  volumeUp(): Promise<ReturnValue | undefined> {
    return this.request('audio/volumeUp');
  }

  volumeDown(): Promise<ReturnValue | undefined> {
    return this.request('audio/volumeDown');
  }

  playMedia(): Promise<ReturnValue | undefined> {
    return this.request('media.controls/play');
  }

  pauseMedia(): Promise<ReturnValue | undefined> {
    return this.request('media.controls/pause');
  }

  stopMedia(): Promise<ReturnValue | undefined> {
    return this.request('media.controls/stop');
  }

  rewindMedia(): Promise<ReturnValue | undefined> {
    return this.request('media.controls/rewind');
  }

  fastForwardMedia(): Promise<ReturnValue | undefined> {
    return this.request('media.controls/fastForward');
  }

  channelUp(): Promise<ReturnValue | undefined> {
    return this.request('tv/channelUp');
  }

  channelDown(): Promise<ReturnValue | undefined> {
    return this.request('tv/channelDown');
  }

  setChannel(
    channelId: string,
  ): Promise<ReturnValue | ErrorReturn | undefined> {
    return this.request('tv/openChannel', { channelId });
  }

  getCurrentChannel(): Promise<ChannelReturn | undefined> {
    return this.request('tv/getCurrentChannel');
  }

  getChannels(): Promise<ErrorReturn | undefined> {
    return this.request('tv/getChannelList');
  }

  getCursorSocket(): Promise<SocketReturn | undefined> {
    return this.request('com.webos.service.networkinput/getPointerInputSocket');
  }

  set3DOn(): Promise<ReturnValue | undefined> {
    return this.request('com.webos.service.tv.display/set3DOn');
  }

  set3DOff(): Promise<ReturnValue | undefined> {
    return this.request('com.webos.service.tv.display/set3DOff');
  }

  getInputs(): Promise<InputListReturn | undefined> {
    return this.request('tv/getExternalInputList');
  }

  setInput(inputName: string): Promise<ReturnValue | ErrorReturn | undefined> {
    return this.request('tv/switchInput', { inputId: inputName });
  }

  getSoftwareInfos(): Promise<SoftwareInfos | undefined> {
    return this.request('com.webos.service.update/getCurrentSWInformation', {});
  }

  getServices(): Promise<ServiceListReturn | undefined> {
    return this.request('api/getServiceList');
  }

  getLaunchPoints(): Promise<LaunchPointsList | undefined> {
    return this.request('com.webos.applicationManager/listLaunchPoints');
  }

  openAppWithPayload(payload: Object): Promise<any> {
    return this.request('com.webos.applicationManager/launch', payload);
  }

  startApp(appId: string): Promise<AppLaunchReturn | ErrorReturn | undefined> {
    return this.request('system.launcher/launch', { id: appId });
  }

  closeApp(appId: string): Promise<ReturnValue | ErrorReturn> {
    return this.request('system.launcher/close', { id: appId });
  }

  getForegroundAppInfo(): Promise<ForegroundApp | ErrorReturn> {
    return this.request('com.webos.applicationManager/getForegroundAppInfo');
  }

  getSoundOutput(): Promise<SoundOutput | ErrorReturn> {
    return this.request('com.webos.service.apiadapter/audio/getSoundOutput');
  }

  getSystemInfo(): Promise<SystemInfo | ErrorReturn> {
    return this.request('system/getSystemInfo');
  }

  getApps(): Promise<AppsList | ErrorReturn> {
    return this.request('com.webos.applicationManager/listApps');
  }

  setSoundOutput(output: string): Promise<ReturnValue | ErrorReturn> {
    return this.request('audio/changeSoundOutput', { output });
  }
}
