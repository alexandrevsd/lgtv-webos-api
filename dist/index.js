"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lgtv2_1 = __importDefault(require("lgtv2"));
const wake_on_lan_1 = __importDefault(require("wake_on_lan"));
class LgTvWebOsApi {
    constructor(ip, macAddress) {
        this.ip = ip;
        this.macAddress = macAddress;
        this.connect();
    }
    connect() {
        this.socket = new lgtv2_1.default({ url: `ws://${this.ip}:3000` });
        this.listenSocket();
    }
    listenSocket() {
        if (this.socket) {
            this.socket.on('error', (e) => console.error(e.message));
            this.socket.on('connect', () => console.log(`Connection to TV at "${this.ip}" established !`));
        }
    }
    request(uri, payload) {
        return new Promise((resolve) => {
            if (this.socket) {
                this.socket.request('ssap://' + uri, payload, (err, res) => {
                    resolve(res);
                });
            }
            else {
                resolve(undefined);
            }
        });
    }
    wakeOnLan() {
        return new Promise((resolve) => {
            if (this.macAddress) {
                wake_on_lan_1.default.wake(this.macAddress, (err) => {
                    if (err)
                        console.error(err);
                    resolve(err);
                });
            }
            else {
                resolve(undefined);
            }
        });
    }
    on(event, callback) {
        if (this.socket) {
            if (event === 'close') {
                this.socket.on(event, callback);
            }
            else if (event === 'connect') {
                this.socket.on(event, callback);
            }
        }
    }
    turnOn() {
        return this.wakeOnLan();
    }
    turnOff() {
        return this.request('system/turnOff');
    }
    openWebBrowserAt(url) {
        return this.request('system.launcher/open', { target: url });
    }
    sendNotification(message) {
        return this.request('system.notifications/createToast', { message });
    }
    setMute(muted) {
        return this.request('audio/setMute', { mute: muted });
    }
    getAudioStatus() {
        return this.request('audio/getStatus');
    }
    getVolume() {
        return this.request('audio/getVolume');
    }
    setVolume(volume) {
        return this.request('audio/setVolume', { volume });
    }
    volumeUp() {
        return this.request('audio/volumeUp');
    }
    volumeDown() {
        return this.request('audio/volumeDown');
    }
    playMedia() {
        return this.request('media.controls/play');
    }
    pauseMedia() {
        return this.request('media.controls/pause');
    }
    stopMedia() {
        return this.request('media.controls/stop');
    }
    rewindMedia() {
        return this.request('media.controls/rewind');
    }
    fastForwardMedia() {
        return this.request('media.controls/fastForward');
    }
    channelUp() {
        return this.request('tv/channelUp');
    }
    channelDown() {
        return this.request('tv/channelDown');
    }
    setChannel(channelId) {
        return this.request('tv/openChannel', { channelId });
    }
    getCurrentChannel() {
        return this.request('tv/getCurrentChannel');
    }
    getChannels() {
        return this.request('tv/getChannelList');
    }
    getCursorSocket() {
        return this.request('com.webos.service.networkinput/getPointerInputSocket');
    }
    set3DOn() {
        return this.request('com.webos.service.tv.display/set3DOn');
    }
    set3DOff() {
        return this.request('com.webos.service.tv.display/set3DOff');
    }
    getInputs() {
        return this.request('tv/getExternalInputList');
    }
    setInput(inputName) {
        return this.request('tv/switchInput', { inputId: inputName });
    }
    getSoftwareInfos() {
        return this.request('com.webos.service.update/getCurrentSWInformation', {});
    }
    getServices() {
        return this.request('api/getServiceList');
    }
    getLaunchPoints() {
        return this.request('com.webos.applicationManager/listLaunchPoints');
    }
    openAppWithPayload(payload) {
        return this.request('com.webos.applicationManager/launch', payload);
    }
    startApp(appId) {
        return this.request('system.launcher/launch', { id: appId });
    }
    closeApp(appId) {
        return this.request('system.launcher/close', { id: appId });
    }
    getForegroundAppInfo() {
        return this.request('com.webos.applicationManager/getForegroundAppInfo');
    }
    getSoundOutput() {
        return this.request('com.webos.service.apiadapter/audio/getSoundOutput');
    }
    getSystemInfo() {
        return this.request('system/getSystemInfo');
    }
    getApps() {
        return this.request('com.webos.applicationManager/listApps');
    }
    setSoundOutput(output) {
        return this.request('audio/changeSoundOutput', { output });
    }
}
exports.default = LgTvWebOsApi;
