# LGTV WebOS API Wrapper

This is a really simple API Wrapper for LGTV WebOS.

## Installation

    npm install lgtv-webos-api

## Import

Typescript :

    import LgTv from 'lgtv-webos-api';

Javascript :

    const LgTv = require('lgtv-webos-api').default;

## Usage

    // MAC ADDRESS is needed only for turn on TV by wake on lan
    const tv = new LgTv('LG_TV_IP_ADDRESS', 'LG_TV_MAC_ADDRESS');

    tv.turnOn(); // This turns the TV ON

## Methods

### turnOn()

Turns TV on

### turnOff()

Turns TV off

### openWebBrowserAt(url: string)

Opens the web browser at specified url

### sendNotification(message: string)

Display a toast notification on the TV

### setMute(muted: boolean)

Turn soud on/off

### getAudioStatus()

Return audio status (like volume, muted...)

### getVolume()

Return volume

### setVolume(volume: number)

Set the volume (between 0 and 100)

### volumeUp()

Increase volume by 1

### volumeDown()

Decrease volume by 1

### playMedia()

Clic play button

### pauseMedia()

Clic pause button

### stopMedia()

Clic stop button

### rewindMedia()

Clic rewind button

### fastForwardMedia()

Clic fast forward button

### channelUp()

Clic channel up button

### channelDown()

Clic channel down button

### setChannel(channelId: string)

Set channel (channelId is actually the number of the channel)

### getCurrentChannel()

Return the actual channel

### getChannels()

Return all channels

### getCursorSocket()

Return cursor socket (to simulate clics on screen)

### set3DOn()

Set 3D on

### set3DOff()

Set 3D off

### getInputs()

Return all inputs available

### setInput(inputName: string)

Set actual TV input

### getSoftwareInfos()

Return TV software infos

### getServices()

Return TV services

### getLaunchPoints()

Return all applications launch points

### openAppWithPayload()

Open an application with payload

### startApp()

Open an application

### closeApp()

Close an application

### getForegroundAppInfo()

Return foreground app infos

### getSoundOutput()

Return actual sound output

### setSoundOutput(output: string)

Set actual sound output

### getSystemInfo()

Return system infos

### getApps()

Return all apps
