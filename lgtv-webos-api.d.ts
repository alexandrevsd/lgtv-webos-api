declare module 'lgtv-webos-api';
import { ErrorCallback } from 'wake_on_lan';
import {
  AppLaunchReturn,
  AppsList,
  AudioStatusReturn,
  ChannelReturn,
  ErrorReturn,
  ForegroundApp,
  InputListReturn,
  LaunchPointsList,
  NotificationReturn,
  ReturnValue,
  ServiceListReturn,
  SocketReturn,
  SoftwareInfos,
  SoundOutput,
  SystemInfo,
} from './src/types';
export default class LgTvWebOsApi {
  private readonly ip;
  private readonly macAddress;
  private socket;
  constructor(ip: string, macAddress?: string);
  private connect;
  private listenSocket;
  private request;
  private wakeOnLan;
  on(event: 'close' | 'connect', callback: () => void): void;
  turnOn(): Promise<ErrorCallback | undefined>;
  turnOff(): Promise<ReturnValue | undefined>;
  openWebBrowserAt(url: string): Promise<AppLaunchReturn | undefined>;
  sendNotification(message: string): Promise<NotificationReturn | undefined>;
  setMute(muted: boolean): Promise<ReturnValue | undefined>;
  getAudioStatus(): Promise<AudioStatusReturn | undefined>;
  getVolume(): Promise<AudioStatusReturn | undefined>;
  setVolume(volume: number): Promise<ReturnValue | undefined>;
  volumeUp(): Promise<ReturnValue | undefined>;
  volumeDown(): Promise<ReturnValue | undefined>;
  playMedia(): Promise<ReturnValue | undefined>;
  pauseMedia(): Promise<ReturnValue | undefined>;
  stopMedia(): Promise<ReturnValue | undefined>;
  rewindMedia(): Promise<ReturnValue | undefined>;
  fastForwardMedia(): Promise<ReturnValue | undefined>;
  channelUp(): Promise<ReturnValue | undefined>;
  channelDown(): Promise<ReturnValue | undefined>;
  setChannel(channelId: string): Promise<ReturnValue | ErrorReturn | undefined>;
  getCurrentChannel(): Promise<ChannelReturn | undefined>;
  getChannels(): Promise<ErrorReturn | undefined>;
  getCursorSocket(): Promise<SocketReturn | undefined>;
  set3DOn(): Promise<ReturnValue | undefined>;
  set3DOff(): Promise<ReturnValue | undefined>;
  getInputs(): Promise<InputListReturn | undefined>;
  setInput(inputName: string): Promise<ReturnValue | ErrorReturn | undefined>;
  getSoftwareInfos(): Promise<SoftwareInfos | undefined>;
  getServices(): Promise<ServiceListReturn | undefined>;
  getLaunchPoints(): Promise<LaunchPointsList | undefined>;
  openAppWithPayload(payload: object): Promise<any>;
  startApp(appId: string): Promise<AppLaunchReturn | ErrorReturn | undefined>;
  closeApp(appId: string): Promise<ReturnValue | ErrorReturn>;
  getForegroundAppInfo(): Promise<ForegroundApp | ErrorReturn>;
  getSoundOutput(): Promise<SoundOutput | ErrorReturn>;
  getSystemInfo(): Promise<SystemInfo | ErrorReturn>;
  getApps(): Promise<AppsList | ErrorReturn>;
  setSoundOutput(output: string): Promise<ReturnValue | ErrorReturn>;
}
